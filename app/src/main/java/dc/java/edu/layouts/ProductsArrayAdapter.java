package dc.java.edu.layouts;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class ProductsArrayAdapter extends ArrayAdapter<ProductModel> {
    private ArrayList<ProductModel> products;
    private Activity context;

    public ProductsArrayAdapter(Activity context) {
        super(context, android.R.layout.simple_list_item_1);
        initialize(new ArrayList<ProductModel>(), context);
    }

    public ProductsArrayAdapter(Activity context, ArrayList<ProductModel> products) {
        super(context, android.R.layout.simple_list_item_1, products);
        initialize(products, context);
    }

    public ProductsArrayAdapter(Activity context, int textViewResourceId, ArrayList<ProductModel> products) {
        super(context, android.R.layout.simple_list_item_1, textViewResourceId, products);
        initialize(products, context);
    }

    private void initialize(ArrayList<ProductModel> products, Activity context) {
        this.products = products;
        this.context = context;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView v = (TextView) super.getView(position, convertView, parent);

        if (v == null) {
            v = new TextView(context);
        }

        v.setTextColor(Color.BLACK);
        v.setText(products.get(position).getName());
        return v;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            v = inflater.inflate(android.R.layout.simple_list_item_1, null);
        }

        TextView lbl = (TextView) v.findViewById(android.R.id.text1);
        lbl.setTextColor(Color.BLACK);
        lbl.setText(products.get(position).getName());

        return v;
    }

    @Nullable
    @Override
    public ProductModel getItem(int position) {
        return products.get(position);
    }
}
