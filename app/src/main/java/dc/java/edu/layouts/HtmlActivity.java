package dc.java.edu.layouts;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class HtmlActivity extends Activity implements Html.ImageGetter {

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_html);

        textView = (TextView) findViewById(R.id.txt_content);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    protected void onStart() {
        super.onStart();

        NewsFeedRequest newsFeedRequest = new NewsFeedRequest(new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray newsArray = new JSONArray(response);
                    JSONObject newsObject = newsArray.getJSONObject(0);
                    final String content = newsObject.getString("description");

                    Spanned spanned = Html.fromHtml(content, HtmlActivity.this, null);
                    textView.setText(spanned);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Toast.makeText(HtmlActivity.this, "Cannot load news feed",
                        Toast.LENGTH_SHORT).show();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(newsFeedRequest);
    }

    @Override
    public Drawable getDrawable(String source) {
        try {
            LevelListDrawable drawable = new LevelListDrawable();
            Bitmap bitmap = new ImageLoadTask().execute(source).get();

            if (bitmap != null) {
                BitmapDrawable bitmapDrawable = new BitmapDrawable(bitmap);
                drawable.addLevel(1, 1, bitmapDrawable);
                drawable.setBounds(0, 0, bitmap. getWidth(), bitmap.getHeight());
                drawable.setLevel(1);
            } else {
                Toast.makeText(HtmlActivity.this, "Cannot load image in loaded HTML",
                        Toast.LENGTH_SHORT).show();
            }

            return drawable;
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    private class ImageLoadTask extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... params) {
            InputStream is = null;
            try {
                is = new URL(params[0]).openStream();

                Bitmap bitmap = BitmapFactory.decodeStream(is);
                is.close();

                return bitmap;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}
