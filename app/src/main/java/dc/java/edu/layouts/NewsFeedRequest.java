package dc.java.edu.layouts;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

public class NewsFeedRequest extends StringRequest {
    public NewsFeedRequest(Response.Listener<String> listener,
                           Response.ErrorListener errorListener) {
        super(Method.POST, "http://13.93.226.244/api/vapi", listener, errorListener);
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        return "action=getOldsNews&page=1&count=1&language=en".getBytes();
    }

    @Override
    public String getBodyContentType() {
        return "application/x-www-form-urlencoded";
    }
}
