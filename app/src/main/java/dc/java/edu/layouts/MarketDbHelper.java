package dc.java.edu.layouts;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import dc.java.edu.layouts.MarketContract.MarketEntry;

public class MarketDbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "MarketDb.db";

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + MarketEntry.TABLE_NAME + " (" +
            MarketEntry._ID + " INTEGER PRIMARY KEY," +
            MarketEntry.COLUMN_NAME_PRODUCT_NAME + " TEXT," +
            MarketEntry.COLUMN_NAME_PRICE + " REAL," +
            MarketEntry.COLUMN_NAME_LOCATION + " TEXT," +
            MarketEntry.COLUMN_NAME_LOC_LAT + " REAL," +
            MarketEntry.COLUMN_NAME_LOC_LONG + " REAL," +
            MarketEntry.COLUMN_NAME_IMAGE + " BLOB)";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + MarketEntry.TABLE_NAME;

    public MarketDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
