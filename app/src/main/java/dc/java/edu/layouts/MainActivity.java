package dc.java.edu.layouts;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends Activity implements View.OnClickListener {

    public static final int REQUEST_FILE_ACCESS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageButton btnAndroid = (ImageButton) findViewById(R.id.btn_android);
        ImageButton btnClock = (ImageButton) findViewById(R.id.btn_android);
        ImageButton btnFingerprint = (ImageButton) findViewById(R.id.btn_fingerprint);
        ImageButton btnPaw = (ImageButton) findViewById(R.id.btn_paw);
        ImageButton btnPerson = (ImageButton) findViewById(R.id.btn_person);
        ImageButton btnPhone = (ImageButton) findViewById(R.id.btn_phone);
        ImageButton btnReport = (ImageButton) findViewById(R.id.btn_report);
        ImageButton btnStop = (ImageButton) findViewById(R.id.btn_stop);

        ImageButton[] buttons = new ImageButton[] {
            btnAndroid, btnClock, btnFingerprint, btnPaw,
            btnPerson, btnPhone, btnReport, btnStop
        };

        for (ImageButton btns : buttons)
            btns.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[] {
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.ACCESS_FINE_LOCATION
            }, REQUEST_FILE_ACCESS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_FILE_ACCESS) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED ) {

            } else {
                finish();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btn_report:
            {
                Intent intent = new Intent(this, HtmlActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_paw:
            {
                Intent intent = new Intent(this, ItemActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_fingerprint:
            {
                Intent intent = new Intent(this, MapActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_clock:
            case R.id.btn_person:
            case R.id.btn_phone:
            case R.id.btn_stop:
            case R.id.btn_android:
            default:
                break;
        }
    }
}
