package dc.java.edu.layouts;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static dc.java.edu.layouts.MarketContract.*;

public class ItemActivity extends Activity implements View.OnClickListener, LocationListener {
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int REQUEST_LOCATION_PERMISSION = 2;
    private static final int REQUEST_CAMERA_PERMISSION = 3;

    private double currentLongitude, currentLatitude;
    private Uri currentPhotoUrl;

    private MarketDbHelper marketDbHelper;

    private ImageView imgProduct;
    private ImageView imgAddImage;
    private Button btnTakePhoto;
    private Button btnSave;
    private EditText txtLocation;
    private EditText txtProductName;
    private EditText txtPrice;
    private ProgressBar progressBarLoad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);

        marketDbHelper = new MarketDbHelper(this);

        imgProduct = (ImageView) findViewById(R.id.img_product);
        imgAddImage = (ImageView) findViewById(R.id.img_add_image);
        btnTakePhoto = (Button) findViewById(R.id.btn_take_photo);
        btnSave = (Button) findViewById(R.id.btn_save);
        txtLocation = (EditText) findViewById(R.id.txt_location);
        txtProductName = (EditText) findViewById(R.id.txt_product_name);
        txtPrice = (EditText) findViewById(R.id.txt_price);
        progressBarLoad = (ProgressBar) findViewById(R.id.progress_bar_load);

        txtLocation.setEnabled(false);

        btnTakePhoto.setOnClickListener(this);
        btnSave.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        setupOnGrantedCameraPermission();
        setupOnGrantedLocationPermission();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED ) {
                setupOnGrantedLocationPermission();
            } else {
                finish();
            }
        } else if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED ) {
                setupOnGrantedCameraPermission();
            } else {
                finish();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            if (resultCode == RESULT_OK) {
                new LoadImageAsync().execute();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[] {
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
            }, REQUEST_LOCATION_PERMISSION);

        } else {
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.removeUpdates(this);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save: {
                Item item = new Item();
                item.productName = txtProductName.getText().toString();
                item.location = txtLocation.getText().toString();
                item.price = txtPrice.getText().toString();
                item.locationLat = currentLatitude;
                item.locationLong = currentLongitude;

                new SaveItemAsync().execute(item);
                break;
            }
            case R.id.btn_take_photo: {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    try {
                        if (currentPhotoUrl != null) {
                            File previousPhotoFile = new File(currentPhotoUrl.getPath());
                            if (previousPhotoFile.exists()) {
                                previousPhotoFile.delete();
                            }
                        }

                        File imageFile = createFileForImage();
                        if (imageFile != null) {
                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                                currentPhotoUrl = Uri.fromFile(imageFile);
                            } else {
                                String provider = getApplicationContext().getPackageName().concat(".provider");
                                currentPhotoUrl = FileProvider.getUriForFile(this, provider, imageFile);
                            }

                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, currentPhotoUrl);
                            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(this, "Cannot create file for image.", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            }
            default:
                break;
        }
    }

    private File createFileForImage() throws IOException {
        String imageFileName = "IMG_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File storageDir = getExternalCacheDir();
        return File.createTempFile(imageFileName, ".jpg", storageDir);
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            Address address = geocoder.getFromLocation(currentLatitude, currentLongitude, 1).get(0);
            String locationName = String.format("%s, %s, %s", address.getAddressLine(0),
                    address.getAddressLine(1), address.getAddressLine(2));

            txtLocation.setText(locationName);
        } catch (IOException e) {
            e.printStackTrace();

            Toast.makeText(this, "Cannot get location name", Toast.LENGTH_SHORT);
        }

        Log.d("ItemActivity_DBG_X", "(loc lat=" + currentLatitude + ", long=" + currentLongitude + ")");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d("ItemActivity_DBG_X", "(loc status changed to " + status);
    }

    @Override
    public void onProviderEnabled(String provider) { }

    @Override
    public void onProviderDisabled(String provider) { }

    private void setupOnGrantedCameraPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[] {
                    Manifest.permission.CAMERA
            }, REQUEST_LOCATION_PERMISSION);
        }
    }

    private void setupOnGrantedLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[] {
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
            }, REQUEST_LOCATION_PERMISSION);

        } else {
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            boolean isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            boolean isNetEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGpsEnabled && !isNetEnabled) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                dialog.setMessage("Location services is not enabled.");
                dialog.setPositiveButton("Open Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                });
                dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                dialog.show();
            }

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0, this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 0, this);
        }
    }

    private class Item {
        String productName;
        String location;
        String price;

        double locationLong;
        double locationLat;
    }

    private class SaveItemAsync extends AsyncTask<Item, Void, Void> {

        private SaveProgressDialogFragment saveProgressDialogFragment;
        private boolean isSUccessful;
        private long count;

        @Override
        protected void onPreExecute() {
            saveProgressDialogFragment = new SaveProgressDialogFragment();
            saveProgressDialogFragment.show(getFragmentManager(), "save_progress");
        }

        @Override
        protected Void doInBackground(Item... params) {
            InputStream in = null;
            ByteArrayOutputStream stream = null;

            try {
                getContentResolver().notifyChange(currentPhotoUrl, null);
                ContentResolver contentResolver = getContentResolver();
                in = contentResolver.openInputStream(currentPhotoUrl);
                if (in == null) throw new NullPointerException();

                stream = new ByteArrayOutputStream();
                Bitmap bitmap = BitmapFactory.decodeStream(in);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
                bitmap.recycle();

                SQLiteDatabase marketDatabase = marketDbHelper.getWritableDatabase();

                Item item = params[0];
                ContentValues values = new ContentValues();
                values.put(MarketEntry.COLUMN_NAME_PRODUCT_NAME, item.productName);
                values.put(MarketEntry.COLUMN_NAME_LOCATION, item.location);
                values.put(MarketEntry.COLUMN_NAME_PRICE, item.price);
                values.put(MarketEntry.COLUMN_NAME_LOC_LAT, currentLatitude);
                values.put(MarketEntry.COLUMN_NAME_LOC_LONG, currentLongitude);
                values.put(MarketEntry.COLUMN_NAME_IMAGE, stream.toByteArray());

                if (BuildConfig.DEBUG) {
                    Log.d("ItemActivity_DBG_X", String.format("(name=%s,loc=%s,price=%s,lat=%f,long=%f)",
                            item.productName, item.location, item.price, currentLatitude, currentLongitude));
                }

                marketDatabase.insert(MarketEntry.TABLE_NAME, null, values);
                marketDatabase.close();
                marketDbHelper.close();

                stream.flush();
                stream.close();

                marketDatabase = marketDbHelper.getReadableDatabase();
                count = DatabaseUtils.queryNumEntries(marketDatabase,
                        MarketEntry.TABLE_NAME);
                marketDatabase.close();
                marketDbHelper.close();

                if (currentPhotoUrl != null) {
                    File previousPhotoFile = new File(currentPhotoUrl.getPath());
                    if (previousPhotoFile.exists()) {
                        previousPhotoFile.delete();
                    }
                }

                System.gc();

                isSUccessful = true;
            } catch (NullPointerException | IOException e) {
                e.printStackTrace();
                isSUccessful = false;
            } finally {
                if (in != null) try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (stream != null) try {
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            saveProgressDialogFragment.dismiss();

            if (isSUccessful) {
                Toast.makeText(ItemActivity.this, "No. of records in database: " + count,
                        Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(ItemActivity.this, "Cannot save data to database.",
                        Toast.LENGTH_SHORT).show();
            }

            finish();
        }
    }

    private class LoadImageAsync extends AsyncTask<Void, Void, Bitmap> {

        @Override
        protected void onPreExecute() {
            imgProduct.setImageBitmap(null);
            imgProduct.setVisibility(View.VISIBLE);

            imgAddImage.setVisibility(View.GONE);
            progressBarLoad.setVisibility(View.VISIBLE);
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            getContentResolver().notifyChange(currentPhotoUrl, null);
            ContentResolver contentResolver = getContentResolver();
            InputStream in;
            Bitmap outBitmap;

            try {
                in = contentResolver.openInputStream(currentPhotoUrl);
                if (in == null) throw new NullPointerException();

                final BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 4;

                outBitmap = BitmapFactory.decodeStream(in, null, options);

                if (BuildConfig.DEBUG) {
                    Log.d("ItemActivity_DBG_X", String.format("is bitmap null=%s", outBitmap == null));
                }

                in.close();
            } catch (IOException e) {
                e.printStackTrace();
                outBitmap = null;
            }

            return outBitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (bitmap == null) {
                Toast.makeText(ItemActivity.this, "Cannot load image.", Toast.LENGTH_SHORT).show();

                imgProduct.setVisibility(View.GONE);
                imgAddImage.setVisibility(View.VISIBLE);
            } else {
                imgProduct.setImageBitmap(bitmap);
            }

            progressBarLoad.setVisibility(View.GONE);
        }
    }
}
