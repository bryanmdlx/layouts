package dc.java.edu.layouts;

import android.provider.BaseColumns;

public class MarketContract {

    private MarketContract() {}

    public static class MarketEntry implements BaseColumns {
        public static final String TABLE_NAME = "entry";
        public static final String COLUMN_NAME_PRODUCT_NAME = "product_name";
        public static final String COLUMN_NAME_LOCATION = "location";
        public static final String COLUMN_NAME_PRICE = "price";
        public static final String COLUMN_NAME_IMAGE = "image";
        public static final String COLUMN_NAME_LOC_LAT = "loc_lat";
        public static final String COLUMN_NAME_LOC_LONG = "loc_long";
    }
}
