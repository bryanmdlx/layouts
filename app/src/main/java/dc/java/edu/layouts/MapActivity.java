package dc.java.edu.layouts;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import static dc.java.edu.layouts.MarketContract.*;

public class MapActivity extends Activity {

    private MapFragment mapFragment;
    private GoogleMap map;

    private Spinner spinner;
    private ProgressBar progressBar;
    private ProductsArrayAdapter spinnerAdapter;
    private MarketDbHelper marketDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        marketDbHelper = new MarketDbHelper(this);

        spinner = (Spinner) findViewById(R.id.products_spinner);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar_load);
        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map_fragment);

        spinnerAdapter = new ProductsArrayAdapter(this, new ArrayList<ProductModel>());
        spinner.setAdapter(spinnerAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                map.clear();

                ProductModel product = (ProductModel) parent.getItemAtPosition(position);
                LatLng latLng = new LatLng(product.getLatitude(), product.getLongitude());
                map.addMarker(new MarkerOptions().position(latLng).title(product.getName()));

                CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(18.0f).build();
                CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                map.moveCamera(cameraUpdate);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = googleMap;
            }
        });

        new PopulateProductsSpinnerTask().execute();
    }

    private class PopulateProductsSpinnerTask extends AsyncTask<Void, Void, Void> {

        private ArrayList<ProductModel> products;

        public PopulateProductsSpinnerTask() {
            products = new ArrayList<>();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            spinner.setEnabled(false);
            progressBar.setVisibility(View.VISIBLE);

            spinnerAdapter.clear();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                SQLiteDatabase marketDatabase = marketDbHelper.getReadableDatabase();

                String selectQuery = "SELECT "
                        + MarketEntry.COLUMN_NAME_PRODUCT_NAME + ", "
                        + MarketEntry.COLUMN_NAME_LOC_LAT + ", "
                        + MarketEntry.COLUMN_NAME_LOC_LONG + ""
                        + " FROM " + MarketEntry.TABLE_NAME;
                Cursor cursor = marketDatabase.rawQuery(selectQuery, null);

                if (cursor.getCount() > 0 && cursor.moveToFirst()) {
                    int productNameColIndex = cursor.getColumnIndex(MarketEntry.COLUMN_NAME_PRODUCT_NAME);
                    int locationColIndex = cursor.getColumnIndex(MarketEntry.COLUMN_NAME_LOCATION);
                    int priceColIndex = cursor.getColumnIndex(MarketEntry.COLUMN_NAME_PRICE);
                    int imageColIndex = cursor.getColumnIndex(MarketEntry.COLUMN_NAME_IMAGE);
                    int latColIndex = cursor.getColumnIndex(MarketEntry.COLUMN_NAME_LOC_LAT);
                    int longColIndex = cursor.getColumnIndex(MarketEntry.COLUMN_NAME_LOC_LONG);

                    do {
                        ProductModel productModel = new ProductModel();
                        productModel.setName(cursor.getString(productNameColIndex));
                        productModel.setLatitude(cursor.getDouble(latColIndex));
                        productModel.setLongitude(cursor.getDouble(longColIndex));
                        products.add(productModel);
                    } while (cursor.moveToNext());
                }

                cursor.close();
                marketDbHelper.close();
            } catch (RuntimeException ex) {
                ex.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            spinnerAdapter.addAll(products);

            spinner.setEnabled(true);
            progressBar.setVisibility(View.GONE);
        }
    }
}
